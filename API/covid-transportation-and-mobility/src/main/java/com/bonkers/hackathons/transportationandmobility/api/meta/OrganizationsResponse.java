package com.bonkers.hackathons.transportationandmobility.api.meta;

import com.bonkers.hackathons.transportationandmobility.api.base.APIResponse;
import lombok.Data;

import java.util.List;

@Data
public class OrganizationsResponse extends APIResponse {

    private List<OrganizationInfo> list;

}
