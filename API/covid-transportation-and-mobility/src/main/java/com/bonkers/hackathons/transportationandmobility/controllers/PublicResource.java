package com.bonkers.hackathons.transportationandmobility.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/public")
public class PublicResource
{

	@GetMapping("/data")
	public Map<String, Object> getData() {
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("message", "Hello there");
		return response;
	}

}
