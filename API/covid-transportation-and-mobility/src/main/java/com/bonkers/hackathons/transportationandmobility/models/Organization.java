package com.bonkers.hackathons.transportationandmobility.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name = "ORGANIZATION", indexes = { @Index(name = "UNQNAMEBRANCH", columnList = "name, branch", unique = true)
})
@Data
public class Organization {

    @Id
    private Integer organizationId;
    private String name;
    private String branch;
    private String displayName;
    private boolean active;
    private boolean authorized;
    private float geoLocationLatitude;
    private float geoLocationLongitude;
    private String address;
    private long createdOn;
    private String createdBy;
    private long modifiedOn;
    private String modifiedBy;

}
