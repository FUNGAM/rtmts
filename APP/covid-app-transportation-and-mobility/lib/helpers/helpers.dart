import 'package:flutter/material.dart';
import 'package:transportationandmobility/models/user_details.dart';

class Helper {
  static Widget geAvatar(UserDetails userDetails) {
    if (userDetails != null) {
      return CircleAvatar(
        radius: 18,
        backgroundImage: NetworkImage(userDetails.photoUrl != null ? userDetails.photoUrl : ""),
      );
    } else {
      return CircleAvatar(
        radius: 18,
      );
    }
  }
}
