package com.bonkers.hackathons.transportationandmobility.repository;

import com.bonkers.hackathons.transportationandmobility.models.Passage;
import com.bonkers.hackathons.transportationandmobility.models.UserInfoDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassageRepository  extends JpaRepository<Passage, Long> {
}
