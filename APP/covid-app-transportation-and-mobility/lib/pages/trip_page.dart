import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:transportationandmobility/bloc/auth_service.dart';
import 'package:transportationandmobility/bloc/location_service.dart';
import 'package:transportationandmobility/helpers/helpers.dart';
import 'package:transportationandmobility/models/user_location.dart';
import 'package:transportationandmobility/utils/sh_navigator.dart';

class TripPage extends StatefulWidget {
  final int ePassId;
  final Key _mapKey = UniqueKey();

  TripPage(this.ePassId);

  @override
  _TripPageState createState() => _TripPageState();
}

class _TripPageState extends State<TripPage> {
  GoogleMapController _controller;
  LocationService locationService;

  static final CameraPosition initialLocation = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 16,
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    if (locationService != null) {
      locationService.stopTracking();
    }
  }

  void _onMapCreate(GoogleMapController controller) {
    this._controller = controller;
    setMapStyle();
  }

  Future<Uint8List> getMarker() async {
    ByteData byteData = await DefaultAssetBundle.of(context).load("assets/images/car.png");
    return byteData.buffer.asUint8List();
  }

  void setMapStyle() async {
    String style = await DefaultAssetBundle.of(context).loadString('assets/maps-style.json');
    _controller.setMapStyle(style);
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthService>(context);
    locationService = Provider.of<LocationService>(context);
    getMarker().then((val) {
      locationService.imageData = val;
    });
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          "User Commute",
          style: TextStyle(
            letterSpacing: 3,
            fontSize: 16,
            color: Colors.blueAccent,
            fontWeight: FontWeight.w900,
          ),
        ),
        actions: <Widget>[
          InkWell(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Helper.geAvatar(authProvider.userDetails),
            ),
            onTap: () => ShNavigator.goToSettingsPage(context),
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
              key: widget._mapKey,
              mapType: MapType.normal,
              initialCameraPosition: initialLocation,
              markers: Set.of((locationService.curLocMarker != null) ? [locationService.curLocMarker] : []),
              circles: Set.of((locationService.geofenceCircles != null) ? locationService.geofenceCircles : []),
              zoomControlsEnabled: false,
              onMapCreated: _onMapCreate),
          Consumer<LocationService>(builder: (context, locationService, _) {
            return !locationService.showAuthMessage
                ? Text("")
                : Container(
                    height: 40,
                    alignment: Alignment.topCenter,
                    decoration: BoxDecoration(color: locationService.isAuthorized ? Colors.green : Colors.redAccent),
                    child: Text(
                      locationService.isAuthorized ? "You are authorized" : "You are Un-Authorized to pass this gate",
                    ),
                  );
          }),
          Consumer<LocationService>(builder: (context, locationService, _) {
            return Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10, bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: InkWell(
                        onTap: () => setUpLocationTracker(locationService),
                        child: Container(
                          height: 50,
                          width: 1000,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30),
                            color: locationService.tripStarted ? Colors.greenAccent[700] : Colors.blueAccent[700],
                          ),
                          child: locationService.buttonLoading
                              ? CircularProgressIndicator(
                                  backgroundColor: Colors.white,
                                )
                              : Text(
                                  locationService.tripStarted ? "End Trip" : "Start Trip",
                                  style: TextStyle(
                                    letterSpacing: 3,
                                    fontSize: 13,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w900,
                                  ),
                                ),
                        ),
                      ),
                    ),
                    SizedBox(width: 20),
                    FloatingActionButton(
                      elevation: 0,
                      onPressed: () => gotoCurrentLocation(locationService),
                      child: Icon(Icons.my_location),
                    ),
                  ],
                ),
              ),
            );
          }),
          Consumer<LocationService>(builder: (context, locationService, _) {
            updateLocationOnMap(locationService.getCurrentLocation(), locationService);
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Latttute: " + (locationService.getCurrentLocation() != null ? locationService.getCurrentLocation().latitude.toString() : "")),
                Text("Longitude: " + (locationService.getCurrentLocation() != null ? locationService.getCurrentLocation().longitude.toString() : "")),
              ],
            );
          }),
        ],
      ),
    );
  }

  void gotoCurrentLocation(LocationService locationService) {
    locationService.getLocation().then((userLocation) => updateLocationOnMap(userLocation, locationService));
  }

  void updateLocationOnMap(UserLocation userLocation, LocationService locationService) {
    if (_controller != null) {
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          new CameraPosition(
            bearing: 192.8334901395799,
            target: LatLng(userLocation.latitude, userLocation.longitude),
            tilt: 0,
            zoom: 16,
          ),
        ),
      );
    }
  }

  void setUpLocationTracker(LocationService locationService) {
    locationService.toggleTracking();
  }
}
