package com.bonkers.hackathons.transportationandmobility.core;

import com.bonkers.hackathons.transportationandmobility.api.base.APIResponseUtil;
import com.bonkers.hackathons.transportationandmobility.api.register.RegisterResponse;
import com.bonkers.hackathons.transportationandmobility.models.*;
import com.bonkers.hackathons.transportationandmobility.repository.EPassRepository;
import com.bonkers.hackathons.transportationandmobility.repository.UserInfoDetailsRespository;
import com.bonkers.hackathons.transportationandmobility.api.register.RegisterService;
import com.bonkers.hackathons.transportationandmobility.api.register.RegisterUserRequest;
import com.bonkers.hackathons.transportationandmobility.repository.UserOrganizationMappingRepository;
import com.bonkers.hackathons.transportationandmobility.repository.UserPassageRepository;
import com.bonkers.hackathons.transportationandmobility.service.UserSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    private UserSessionService userSessionService;

    @Autowired
    private EPassRepository ePassRepository;

    @Autowired
    private UserInfoDetailsRespository userInfoDetailsRespository;

    @Autowired
    private UserOrganizationMappingRepository userOrganizationMappingRepository;

    @Autowired
    private UserPassageRepository userPassageRepository;

    @Override
    public RegisterResponse registerUser(RegisterUserRequest request) {

        RegisterResponse response = new RegisterResponse();

        try {
            if(!request.validate()) {
                System.out.println("Invalid register request " + request.toString());
                APIResponseUtil.error(response, "Please provide valid 10 digit phone number and your registred organization(s) from list");
                return response;
            }

            UserInfo user = userSessionService.getUserInfo();
            if(null == user) {
                System.out.println("Invalid logged in user: ");
                APIResponseUtil.error(response, "Error validating user for registration");
                return response;
            }
            if(userInfoDetailsRespository.findById(user.getUid()).isPresent()) {
                // user already registred
                EPass ePass = generateEpass(user.getUid());
                response.setEpassId(ePass.getEpassId());
            } else {
                UserInfoDetails userInfoDetails = getUserInfoDetails(user, request);
                EPass ePass = generateEpass(user.getUid());
                List<String> organizationNames = generateUserOrgMapping(request, userInfoDetails, ePass.getEpassId());
                mapUserToPassages(userInfoDetails, ePass.getEpassId());
                response.setEpassId(ePass.getEpassId());
                response.setOrganizationNames(organizationNames);
            }

        }
        catch (Exception e) {
            System.out.println("Error registering user at RTMTS: " + e.getMessage());
            APIResponseUtil.error(response, "Error registering at RTMTS. Please try again or contact support team if issue exists");
            return response;
        }
        APIResponseUtil.success(response);
        return response;
    }

    private UserInfoDetails getUserInfoDetails (UserInfo user,
                                                RegisterUserRequest request)
    {
        Optional<UserInfoDetails> userInfoDetails = userInfoDetailsRespository.findById(
            user.getUid());

        if (!userInfoDetails.isPresent()) {
            return userInfoDetailsRespository.save(new UserInfoDetails(
                user.getUid(),
                user.getEmail(),
                request.getPhoneNumber(),
                request.getDeviceId(),
                true,
                true));
        }

        return userInfoDetails.get();
    }

    private EPass generateEpass(String userId) {

        EPass ePass = ePassRepository.findByUserId(userId);
        if(ePass == null) {
            ePass = new EPass();
            ePass.setUserId(userId);
            ePass.setActive(true);
            ePass.setCreatedBy(userId);
            ePass.setCreatedOn(System.currentTimeMillis());
            ePass.setModifiedBy(userId);
            ePass.setModifiedOn(System.currentTimeMillis());
            ePassRepository.save(ePass);
        }
        return ePass;
    }

    private List<String> generateUserOrgMapping (RegisterUserRequest request,
                                                 UserInfoDetails userInfoDetails,
                                                 Integer ePassId) {

        List<String> organizationNames = new ArrayList<>();
        List<UserOrganizationMapping> userOrgs = new ArrayList<>();

        UserOrganizationMapping userOrganizationMapping = new UserOrganizationMapping();
        userOrganizationMapping.setEpassId(ePassId);
        userOrganizationMapping.setOrganizationId(1);
        userOrganizationMapping.setUserId(userInfoDetails.getUserId());
        userOrganizationMapping.setActive(true);
        userOrganizationMapping.setCreatedBy(userInfoDetails.getUserId());
        userOrganizationMapping.setCreatedOn(System.currentTimeMillis());
        userOrganizationMapping.setModifiedBy(userInfoDetails.getUserId());
        userOrganizationMapping.setModifiedOn(System.currentTimeMillis());

        userOrgs.add(userOrganizationMapping);
        organizationNames.add("Manipal Hospital - Whitefield Bangalore");

        userOrganizationMapping = new UserOrganizationMapping();
        userOrganizationMapping.setEpassId(ePassId);
        userOrganizationMapping.setUserId(userInfoDetails.getUserId());
        userOrganizationMapping.setOrganizationId(2);
        userOrganizationMapping.setActive(true);
        userOrganizationMapping.setCreatedBy(userInfoDetails.getUserId());
        userOrganizationMapping.setCreatedOn(System.currentTimeMillis());
        userOrganizationMapping.setModifiedBy(userInfoDetails.getUserId());
        userOrganizationMapping.setModifiedOn(System.currentTimeMillis());

        userOrgs.add(userOrganizationMapping);
        organizationNames.add("Manipal Hospital - Majestic Bangalore");

        userOrganizationMappingRepository.saveAll(userOrgs);
        return organizationNames;
    }

    private void mapUserToPassages(UserInfoDetails userInfoDetails, Integer ePassId) {

        List<UserPassage> userPassages = new ArrayList<>();

        UserPassage userPassage = new UserPassage();
        userPassage.setEpassId(ePassId);
        userPassage.setUserId(userInfoDetails.getUserId());
        userPassage.setPassageId(1);
        userPassage.setActive(true);
        userPassage.setCreatedBy(userInfoDetails.getUserId());
        userPassage.setCreatedOn(System.currentTimeMillis());
        userPassage.setModifiedBy(userInfoDetails.getUserId());
        userPassage.setModifiedOn(System.currentTimeMillis());
        userPassages.add(userPassage);

        userPassage = new UserPassage();
        userPassage.setEpassId(ePassId);
        userPassage.setUserId(userInfoDetails.getUserId());
        userPassage.setPassageId(2);
        userPassage.setActive(true);
        userPassage.setCreatedBy(userInfoDetails.getUserId());
        userPassage.setCreatedOn(System.currentTimeMillis());
        userPassage.setModifiedBy(userInfoDetails.getUserId());
        userPassage.setModifiedOn(System.currentTimeMillis());
        userPassages.add(userPassage);

        userPassageRepository.saveAll(userPassages);
    }
}
