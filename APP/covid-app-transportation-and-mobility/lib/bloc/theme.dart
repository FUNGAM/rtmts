import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeChanger with ChangeNotifier {
  ThemeData _themeData;
  bool isDarkMode = false;
  static final prefsIsDarkMode = "isDarkMode";
  static final fontFamily = "Montserrat";

  final darkTheme = ThemeData(
    brightness: Brightness.dark,
    primaryColor: Colors.grey[900],
    accentColor: Colors.deepOrangeAccent,
    fontFamily: fontFamily,
  );

  final lightTheme = ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.blueAccent,
    accentColor: Colors.deepOrangeAccent,
    fontFamily: fontFamily,
  );

  ThemeChanger() {
    getThemeFromPrefs();
  }

  getTheme() => _themeData;

  toggleDarkMode() {
    isDarkMode = !isDarkMode;
    setThemeIntoPrefs(isDarkMode);
    setThemeData(isDarkMode);
  }

  setThemeData(bool isDarkMode) {
    if (isDarkMode) {
      _themeData = darkTheme;
    } else {
      _themeData = lightTheme;
    }
    notifyListeners();
  }

  setThemeIntoPrefs(bool isDarkMode) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool(prefsIsDarkMode, isDarkMode);
  }

  getThemeFromPrefs() async {
    final prefs = await SharedPreferences.getInstance();
    bool spmode = prefs.getBool(prefsIsDarkMode);
    if (spmode == null) {
      await setThemeIntoPrefs(isDarkMode);
    } else {
      isDarkMode = spmode;
    }
    setThemeData(isDarkMode);
  }
}
