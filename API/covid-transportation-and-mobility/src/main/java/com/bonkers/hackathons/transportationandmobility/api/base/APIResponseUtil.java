package com.bonkers.hackathons.transportationandmobility.api.base;

import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

public class APIResponseUtil {

    public static void error (APIResponse response)
    {
        response.setStatus(HttpStatus.BAD_REQUEST);
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());
    }

    public static void success (APIResponse response)
    {
        response.setStatus(HttpStatus.OK);
        response.setStatusCode(HttpStatus.OK.value());
    }

    public static void notImplemented (APIResponse response)
    {
        response.setStatus(HttpStatus.NOT_IMPLEMENTED);
        response.setStatusCode(HttpStatus.NOT_IMPLEMENTED.value());
    }

    public static void error(APIResponse response, List<ErrorMessage> messages) {
        response.setStatus(HttpStatus.BAD_REQUEST);
        response.addErrorMessage(messages);
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());
    }

    public static void notImplemented(APIResponse response, List<ErrorMessage> messages) {
        response.setStatus(HttpStatus.NOT_IMPLEMENTED);
        response.addErrorMessage(messages);
        response.setStatusCode(HttpStatus.NOT_IMPLEMENTED.value());
    }

    public static void error(APIResponse response, String message) {
        response.setStatus(HttpStatus.BAD_REQUEST);
        List<ErrorMessage> errorMessageList = new ArrayList<>();
        addErrorMessageToList(message, errorMessageList);
        response.addErrorMessage(errorMessageList);
        response.setStatusCode(HttpStatus.BAD_REQUEST.value());
    }

    public static void notImplemented(APIResponse response, String message) {
        response.setStatus(HttpStatus.NOT_IMPLEMENTED);
        List<ErrorMessage> errorMessageList = new ArrayList<>();
        addErrorMessageToList(message, errorMessageList);
        response.addErrorMessage(errorMessageList);
        response.setStatusCode(HttpStatus.NOT_IMPLEMENTED.value());
    }

    private static void addErrorMessageToList(String message, List<ErrorMessage> errorMessageList) {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setErrorType(ErrorType.Error);
        errorMessage.setMessage(message);
        errorMessageList.add(errorMessage);
    }

}
