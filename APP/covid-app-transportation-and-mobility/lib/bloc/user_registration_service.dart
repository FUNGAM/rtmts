import 'dart:convert';

import 'package:device_id/device_id.dart';
import 'package:flutter/material.dart';
import 'package:transportationandmobility/helpers/environment.dart';
import 'package:transportationandmobility/helpers/network_util.dart';
import 'package:transportationandmobility/models/epass_response.dart';
import 'package:transportationandmobility/models/org_response.dart';
import 'package:transportationandmobility/models/organization.dart';
import 'package:transportationandmobility/models/user_request.dart';

class UserRegistrationService extends ChangeNotifier {
  String phoneNumber;
  Organization org;
  List<Organization> orgs = [];
  int ePassId;

  Future<OrgResponse> getOrganizations() async {
    var uri = NetworkUtil.getUrl(Environment.contextUrl, '/meta/organizations');
    var response = await NetworkUtil.get(uri);
    OrgResponse item = OrgResponse.fromJson(jsonDecode(response));
    orgs = item.list;
    org = orgs[0];
    notifyListeners();
    return item;
  }

  Future<void> registerUserAndGeneratePass() async {
    String deviceId = await DeviceId.getID;
    var uri = NetworkUtil.getUrl(Environment.contextUrl, '/register/user');
    UserRequest request = UserRequest(
      phoneNumber: phoneNumber,
      organizationId: [org.organizationId],
      deviceId: deviceId,
    );
    var response = await NetworkUtil.post(uri, body: jsonEncode(request));
    EpassResponse item = EpassResponse.fromJson(jsonDecode(response));
    ePassId = item.epassId;
    notifyListeners();
    return item;
  }
}
