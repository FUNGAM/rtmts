import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:transportationandmobility/helpers/environment.dart';
import 'package:transportationandmobility/helpers/network_util.dart';
import 'package:transportationandmobility/models/passage_info.dart';
import 'package:transportationandmobility/models/passage_response.dart';
import 'package:transportationandmobility/models/user_location.dart';

class LocationService extends ChangeNotifier {
  UserLocation _currentLocation = UserLocation(latitude: 12.924, longitude: 77.6637);
  Uint8List imageData;
  List<Circle> geofenceCircles = [];
  Marker curLocMarker;
  bool tripStarted = false;
  bool buttonLoading = false;
  List<PassageInfo> geofence = [];
  int ePassId;
  bool showAuthMessage = false;
  bool isAuthorized = false;
  PassageInfo currentPassage;

  var locator = Geolocator();
  var locationOptions = LocationOptions(accuracy: LocationAccuracy.bestForNavigation, distanceFilter: 1);

  StreamController<UserLocation> _locationController = StreamController<UserLocation>.broadcast();

  Stream<UserLocation> get locationStream => _locationController.stream;

  StreamSubscription locationSubscription;

  LocationService() {}

  void stopTracking() {
    if (locationSubscription != null && !locationSubscription.isPaused) {
      locationSubscription.cancel();
    }
  }

  void toggleTracking() {
    buttonLoading = true;
    notifyListeners();
    tripStarted = !tripStarted;
    tripStarted ? startTracking() : stopTracking();
    buttonLoading = false;
    notifyListeners();
  }

  void startTracking() {
    locationSubscription = locator.getPositionStream(locationOptions).listen((Position position) {
      if (!_locationController.isClosed) {
        _currentLocation = UserLocation(
          longitude: position.longitude,
          latitude: position.latitude,
          heading: position.heading,
        );
        _locationController.add(_currentLocation);
        updateMarkerAndCircle(_currentLocation);

        geofence.forEach((loc) {
          locator.distanceBetween(position.latitude, position.longitude, loc.latitude, loc.longitude).then((distance) async {
            if (distance <= loc.radius) {
              currentPassage = loc;
              print('entered the location:' + loc.address + ' distabce: ' + distance.toString());
              showAuthMessage = true;
              if (loc.userAuthorized) {
                isAuthorized = loc.userAuthorized;
              } else {
                isAuthorized = loc.userAuthorized;
                showAuthMessage = true;
                locationSubscription.cancel();
              }
              await checkPassageIsAuthorized(loc);
            }
          });
        });

        if (currentPassage != null) {
          locator
              .distanceBetween(
            position.latitude,
            position.longitude,
            currentPassage.latitude,
            currentPassage.longitude,
          )
              .then((distance) async {
            if (distance > currentPassage.radius) {
              showAuthMessage = false;
            }
          });
        }

        notifyListeners();
      }
    });
  }

  Future<bool> checkPassageIsAuthorized(PassageInfo loc) async {
    var uri = NetworkUtil.getUrl(Environment.contextUrl, '/device/status', {
      'epassId': ePassId.toString(),
      'passageId': loc.passageId.toString(),
    });
    var response = await NetworkUtil.get(uri);
    PassageResponse item = PassageResponse.fromJson(jsonDecode(response));
    geofence = item.passages;
    return true;
  }

  void updateMarkerAndCircle(UserLocation userLocation) {
    LatLng latlng = LatLng(userLocation.latitude, userLocation.longitude);
    curLocMarker = Marker(
      markerId: MarkerId("home"),
      position: latlng,
      rotation: userLocation.heading,
      draggable: false,
      zIndex: 2,
      flat: true,
      anchor: Offset(0.5, 0.5),
      icon: BitmapDescriptor.fromBytes(imageData),
    );
  }

  UserLocation getCurrentLocation() {
    return _currentLocation;
  }

  Future<UserLocation> getLocation() async {
    try {
      Position position = await locator.getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);
      _currentLocation = UserLocation(
        longitude: position.longitude,
        latitude: position.latitude,
        heading: position.heading,
        accuracy: position.accuracy,
      );
      updateMarkerAndCircle(_currentLocation);
      notifyListeners();
      return _currentLocation;
    } catch (e) {
      print("Filed to fetch current location");
    }
  }

  Future<void> initAllGiofence(int ePassId) async {
    this.ePassId = ePassId;
    var locations = await getGeofences();
    geofenceCircles = [];
    locations.forEach((loc) {
      geofenceCircles.add(getCircle(
        LatLng(loc.latitude, loc.longitude),
        loc.radius,
        loc.passageId.toString(),
        loc.userAuthorized,
      ));
    });
    notifyListeners();
    return;
  }

  Circle getCircle(LatLng latlon, double radius, String id, bool userAuthorized) {
    var color = userAuthorized ? Colors.blue.withAlpha(70) : Colors.red[100];
    return Circle(
      circleId: CircleId(id),
      radius: radius,
      zIndex: 1,
      strokeColor: color,
      center: latlon,
      strokeWidth: 10,
      fillColor: color,
    );
  }

  Future<List<PassageInfo>> getGeofences() async {
    var uri = NetworkUtil.getUrl(Environment.contextUrl, '/meta/passages');
    var response = await NetworkUtil.get(uri);
    PassageResponse item = PassageResponse.fromJson(jsonDecode(response));
    geofence = item.passages;
    return geofence;
  }
}
