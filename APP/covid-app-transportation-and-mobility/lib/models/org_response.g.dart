// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'org_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrgResponse _$OrgResponseFromJson(Map<String, dynamic> json) {
  return OrgResponse(
    status: json['status'] as String,
    statusCode: json['statusCode'] as int,
    list: (json['list'] as List)
        ?.map((e) =>
            e == null ? null : Organization.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$OrgResponseToJson(OrgResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'statusCode': instance.statusCode,
      'list': instance.list,
    };
