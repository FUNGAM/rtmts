package com.bonkers.hackathons.transportationandmobility.models;

import com.google.firebase.auth.FirebaseToken;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Date;


@Entity
@Table(name = "USER_INFO_DETAILS", indexes = { @Index(name = "UNQEMAIL", columnList = "emailAddress", unique = true),
        @Index(name = "UNQPHONE", columnList = "phoneNumber", unique = true)
})
@Data
public class UserInfoDetails {

    @Id
    private String userId;
    private String emailAddress;
    private String phoneNumber;
    private String deviceId;
    private String alternateEmailAddress;
    private String alternatePhoneNumber;
    private boolean active;
    private boolean authorized;
    private String address;
    private long createdOn;
    private String createdBy;
    private long modifiedOn;
    private String modifiedBy;

    public UserInfoDetails(){}

    public UserInfoDetails (String userId, String emailAddress, String phoneNumber, String deviceId, boolean active, boolean authorized)
    {
        this.userId =  userId;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.deviceId = deviceId;
        this.active = active;
        this.authorized = authorized;
        this.address = "Bangalore 560048";
        this.createdOn = System.currentTimeMillis();
        this.createdBy = userId;
        this.modifiedOn = System.currentTimeMillis();
        this.modifiedBy = userId;
    }
}
