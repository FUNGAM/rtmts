package com.bonkers.hackathons.transportationandmobility.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name = "USER_PASSAGE",
        indexes = { @Index(name = "UNQUSERPASSAGE", columnList = "epassId, passageId", unique = true)
        })
@Data
public class UserPassage {

    @Id
    @GeneratedValue
    private Integer userPassageId;
    private Integer epassId;
    private String userId;
    private Integer passageId;
    private boolean active;
    private long createdOn;
    private String createdBy;
    private long modifiedOn;
    private String modifiedBy;
}
