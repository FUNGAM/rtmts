import 'package:json_annotation/json_annotation.dart';

import 'organization.dart';

part 'org_response.g.dart';

@JsonSerializable()
class OrgResponse {
  String status;
  int statusCode;
  List<Organization> list;

  OrgResponse({this.status, this.statusCode, this.list});

  factory OrgResponse.fromJson(Map<String, dynamic> json) => _$OrgResponseFromJson(json);

  Map<String, dynamic> toJson() => _$OrgResponseToJson(this);
}
