package com.bonkers.hackathons.transportationandmobility.repository;

import com.bonkers.hackathons.transportationandmobility.models.UserOrganizationMapping;
import com.bonkers.hackathons.transportationandmobility.models.UserPassage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserPassageRepository extends JpaRepository<UserPassage, Long> {

    String TableName = "UserPassage";

    String findByEpassId = "select up.passageId from " + TableName +
            " up where up.epassId = :epassId " +
            " and up.active = true";

    @Query(findByEpassId)
    List<Integer> findByEpassId (@Param("epassId") Integer epassId);

    String findByEpassAndPassageId = "select up from " + TableName +
            " up where up.epassId = :epassId " +
            " and up.passageId = :passageId" +
            " and up.active = 1";

    @Query(findByEpassAndPassageId)
    UserPassage findByEpassAndPassageId (@Param("epassId") Integer epassId,
                                 @Param("passageId") Integer passageId);
}
