package com.bonkers.hackathons.transportationandmobility.api.device;

import com.bonkers.hackathons.transportationandmobility.api.base.APIResponse;
import lombok.Data;

@Data
public class DeviceStatusResponse extends APIResponse {

    private Integer epassId;
    private Integer passageId;
    private boolean authorized;

}
