package com.bonkers.hackathons.transportationandmobility.api.device;

public interface ValidateDeviceService {

    Boolean validateDeviceId(String deviceId);
}
