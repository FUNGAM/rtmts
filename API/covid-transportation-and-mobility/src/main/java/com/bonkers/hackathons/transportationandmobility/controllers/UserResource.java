package com.bonkers.hackathons.transportationandmobility.controllers;

import com.bonkers.hackathons.transportationandmobility.models.UserInfo;
import com.bonkers.hackathons.transportationandmobility.service.UserSessionService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserResource
{

	@Autowired
	UserSessionService userSessionService;

	@GetMapping("/me")
	public UserInfo userinfo () {
		return userSessionService.getUserInfo();
	}

	@GetMapping("/create/token")
	public String getCustomToken() throws FirebaseAuthException {
		return FirebaseAuth.getInstance().createCustomToken(userSessionService.getUserId());
	}

}
