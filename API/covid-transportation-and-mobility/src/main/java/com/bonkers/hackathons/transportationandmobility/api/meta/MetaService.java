package com.bonkers.hackathons.transportationandmobility.api.meta;

public interface MetaService {

    OrganizationsResponse getAllRegisteredOrganizations();

    PassagesResponse getAllAndUserPassages();
}
