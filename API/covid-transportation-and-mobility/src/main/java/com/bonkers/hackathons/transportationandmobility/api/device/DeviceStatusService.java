package com.bonkers.hackathons.transportationandmobility.api.device;

public interface DeviceStatusService {

    DeviceStatusResponse deviceStatus (Integer ePassId, Integer passageId);
}
