package com.bonkers.hackathons.transportationandmobility.api.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

public class ErrorMessage {

    @XmlElement(name="ErrorType")
    private ErrorType errorType;
    @XmlElement(name="Message")
    private String message;

    public ErrorType getErrorType()
    {
        return errorType;
    }

    public void setErrorType(ErrorType errorType)
    {
        this.errorType = errorType;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    @Override
    public String toString ()
    {
        return "ErrorMessage{" +
                "errorType='" + errorType + '\'' +
                ", message=" + message +
                '}';
    }

    @Override public boolean equals (Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ErrorMessage that = (ErrorMessage) o;
        return errorType == that.errorType && java.util.Objects.equals(message, that.message);
    }

    @Override public int hashCode ()
    {
        return java.util.Objects.hash(errorType, message);
    }

}
