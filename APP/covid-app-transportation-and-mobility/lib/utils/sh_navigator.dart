import 'package:flutter/material.dart';
import 'package:transportationandmobility/pages/settings_page.dart';
import 'package:transportationandmobility/pages/trip_page.dart';

class ShNavigator {
  static void goToSettingsPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SettingsPage(),
      ),
    );
  }

  static void goToTripManagePage(BuildContext context, int ePassId) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => TripPage(ePassId),
      ),
    );
  }
}
