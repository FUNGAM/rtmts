package com.bonkers.hackathons.transportationandmobility.core;

import com.bonkers.hackathons.transportationandmobility.api.base.APIResponseUtil;
import com.bonkers.hackathons.transportationandmobility.api.device.ValidateDeviceService;
import com.bonkers.hackathons.transportationandmobility.api.meta.*;
import com.bonkers.hackathons.transportationandmobility.models.*;
import com.bonkers.hackathons.transportationandmobility.repository.EPassRepository;
import com.bonkers.hackathons.transportationandmobility.repository.OrganizationRepository;
import com.bonkers.hackathons.transportationandmobility.repository.PassageRepository;
import com.bonkers.hackathons.transportationandmobility.repository.UserPassageRepository;
import com.bonkers.hackathons.transportationandmobility.service.UserSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MetaServiceImpl implements MetaService {

    @Autowired
    private UserSessionService userSessionService;

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private PassageRepository passageRepository;

    @Autowired
    private EPassRepository ePassRepository;

    @Autowired
    private UserPassageRepository userPassageRepository;

    @Autowired
    private ValidateDeviceService validateDeviceService;

    @Override
    public OrganizationsResponse getAllRegisteredOrganizations()
    {
        // Declaration
        OrganizationsResponse response = new OrganizationsResponse();
        List<OrganizationInfo> OrganizationInfoList;

        OrganizationInfoList = new ArrayList<>();
        List<Organization> orgList = organizationRepository.findAll();
        for(Organization org: orgList) {
            OrganizationInfo orgInfo = new OrganizationInfo(org);
            OrganizationInfoList.add(orgInfo);
        }
        response.setList(OrganizationInfoList);
        APIResponseUtil.success(response);
        return response;
    }

    @Override
    public PassagesResponse getAllAndUserPassages()
    {
        // Declaration
        PassagesResponse response = new PassagesResponse();
        List<PassageInfo> passageInfoList;
        UserInfo user;
        boolean isValidDeviceUserPair;

        isValidDeviceUserPair = validateDeviceService.validateDeviceId(userSessionService.getDeviceId());
        if(!isValidDeviceUserPair) {
            System.out.println("User is not authorized for device to use device: " + userSessionService.getDeviceId() );
            APIResponseUtil.error(response, "You are not authorized to use this device");
            return response;
        }

        user = userSessionService.getUserInfo();
        if(null == user) {
            System.out.println("Invalid logged in user: ");
            APIResponseUtil.error(response, "Error validating user");
            return response;
        }

        passageInfoList = new ArrayList<>();
        List<Passage> passages = passageRepository.findAll();

        EPass ePass = ePassRepository.findByUserId(user.getUid());
        if(null == ePass) {
            System.out.println("No Epass generated for user: " + user.getUid() + ", email: " +user.getEmail());
            APIResponseUtil.error(response, "No Epass generated for you yet!");
            return response;
        }
        List<Integer> userPassageIdList = userPassageRepository.findByEpassId(ePass.getEpassId());
        for(Passage p: passages) {
            PassageInfo passageInfo = new PassageInfo(p);
            if(userPassageIdList.contains(passageInfo.getPassageId())) {
                passageInfo.setUserAuthorized(true);
            }
            passageInfoList.add(passageInfo);
        }

        response.setPassages(passageInfoList);
        APIResponseUtil.success(response);
        return response;
    }
}
