import 'dart:async';

import 'package:http_interceptor/http_with_interceptor.dart';
import 'package:transportationandmobility/bloc/auth_interceptor.dart';

import 'environment.dart';

class NetworkUtil {
  static Uri getUrl(String contextUrl, String url, [Map<String, String> queryParameters]) {
    if (Environment.isHttps) {
      return Uri.https(contextUrl, url, queryParameters);
    } else {
      return Uri.http(contextUrl, url, queryParameters);
    }
  }

  static Future<dynamic> get(var url) async {
    HttpWithInterceptor http = HttpWithInterceptor.build(interceptors: [AuthInterceptor()]);

    try {
      var response = await http.get(url.toString());

      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw Exception("Error while fetching. \n ${response.body}");
      }
    } catch (e) {
      return Future.value(null);
    }
  }

  static Future<dynamic> post(var url, {body, encoding}) async {
    HttpWithInterceptor http = HttpWithInterceptor.build(interceptors: [AuthInterceptor()]);

    try {
      var response = await http.post(url.toString(), body: body, headers: {"Accept": "application/json", "content-type": "application/json"});

      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw Exception("Error while fetching. \n ${response.body}");
      }
    } catch (e) {
      throw e;
    }
  }
}
