# RTMTS

RTMTS 

## Problem Statement
Transportation and Mobility: During the lockdown, people are not allowed to travel except for those working in essential services, e.g., doctors, nurses going to their offices. But how do you identify who are “essential travellers” or those just roaming around for fun? Come up with solutions that allow people travelling to their places of work hassle-free and limit the movement of others

## Solution
Online real time GPS based Movement Tracking System (RTMTS) for hassle free movement of authenticate essential workers and at the same time allowing police force to invest more time on non-authenticate people for manual checks.
Every authenticated person has to register his/her phone number and GPS device (smartphone) at RTMTS.
The idea involves the temporary installation of narrow passages on roads (under service) at pre selected locations listed by the respective police dept. 
These passages will allow smooth movement of authenticated person through automated installed sensor based gates by fetching data about approaching authorized travelers one at a time through the passage, while restricting others for manual checking by police force.
