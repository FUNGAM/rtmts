import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:transportationandmobility/bloc/auth_service.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthService>(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          "Settings",
          style: TextStyle(
            letterSpacing: 3,
            fontSize: 16,
            color: Colors.blueAccent,
            fontWeight: FontWeight.w900,
          ),
        ),
      ),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.only(left: 20, top: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Consumer<AuthService>(builder: (context, authProvider, _) {
              return Column(
                children: <Widget>[
                  CircleAvatar(
                    radius: 60,
                    backgroundImage: NetworkImage(authProvider.userDetails != null ? authProvider.userDetails.photoUrl : ""),
                  ),
                  SizedBox(height: 15),
                  Text(authProvider.userDetails != null ? authProvider.userDetails.fullName : ""),
                  SizedBox(height: 15),
                  Text(authProvider.userDetails != null ? authProvider.userDetails.email : ""),
                  SizedBox(height: 40),
                ],
              );
            }),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 5),
                Divider(),
                SizedBox(height: 5),
                InkWell(
                  child: Text(
                    "Log out",
                    style: TextStyle(
                      color: Theme.of(context).accentColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                    ),
                  ),
                  onTap: () {
                    authProvider.logOut();
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
