package com.bonkers.hackathons.transportationandmobility.core;

import com.bonkers.hackathons.transportationandmobility.api.device.ValidateDeviceService;
import com.bonkers.hackathons.transportationandmobility.models.UserInfoDetails;
import com.bonkers.hackathons.transportationandmobility.repository.UserInfoDetailsRespository;
import com.bonkers.hackathons.transportationandmobility.service.UserSessionService;
import com.bonkers.hackathons.transportationandmobility.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ValidateDeviceServiceImpl implements ValidateDeviceService {

    @Autowired
    private UserSessionService userSessionService;

    @Autowired
    private UserInfoDetailsRespository userInfoDetailsRespository;
    @Override
    public Boolean validateDeviceId(String deviceId) {

        if(!StringUtil.nullOrEmptyOrBlankString(deviceId)) {

            String userId = userSessionService.getUserId();
            Optional<UserInfoDetails> userInfoDetails = userInfoDetailsRespository.findById(userId);

            if(userInfoDetails.isPresent()) {
                if(userInfoDetails.get().getDeviceId().contentEquals(deviceId)) {
                    return true;
                }
            }
        }
        return false;
    }
}
