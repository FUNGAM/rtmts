import 'package:json_annotation/json_annotation.dart';

part 'user_request.g.dart';

@JsonSerializable()
class UserRequest {
  String phoneNumber;
  String deviceId;
  List<int> organizationId;

  factory UserRequest.fromJson(Map<String, dynamic> json) => _$UserRequestFromJson(json);

  UserRequest({
    this.phoneNumber,
    this.deviceId,
    this.organizationId,
  });

  Map<String, dynamic> toJson() => _$UserRequestToJson(this);
}
