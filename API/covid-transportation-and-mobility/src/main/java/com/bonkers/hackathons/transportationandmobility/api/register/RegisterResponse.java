package com.bonkers.hackathons.transportationandmobility.api.register;

import com.bonkers.hackathons.transportationandmobility.api.base.APIResponse;
import lombok.Data;

import java.util.List;

@Data
public class RegisterResponse extends APIResponse {

    private Integer EpassId;
    private List<String> organizationNames;
}
