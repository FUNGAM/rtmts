package com.bonkers.hackathons.transportationandmobility.api.register;

import com.bonkers.hackathons.transportationandmobility.util.StringUtil;
import lombok.Data;

import java.util.List;

@Data
public class RegisterUserRequest {

    private String phoneNumber;
    private String alternateEmailAddress;
    private String alternatePhoneNumber;
    private List<String> organizationId;
    private String deviceId;

    public boolean validate() {

        return !(StringUtil.nullOrEmptyOrBlankString(phoneNumber)
                || phoneNumber.length() != 10
                || StringUtil.nullOrEmptyOrBlankString(deviceId)
                || organizationId == null
                || organizationId.isEmpty());
    }
}
