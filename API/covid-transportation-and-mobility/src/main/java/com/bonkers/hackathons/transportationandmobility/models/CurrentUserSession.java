package com.bonkers.hackathons.transportationandmobility.models;

import lombok.Data;

import java.util.UUID;

@Data
public class CurrentUserSession
{

    public CurrentUserSession ()
    {
        this.requestUUID = UUID.randomUUID().toString();
    }

    private String requestUUID;
    private UserInfo userInfo;
    private String deviceId;
}
