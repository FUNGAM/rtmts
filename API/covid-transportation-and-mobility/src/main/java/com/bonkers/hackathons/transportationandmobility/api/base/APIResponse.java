package com.bonkers.hackathons.transportationandmobility.api.base;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;


@XmlType(name = "APIResponse")
@XmlRootElement(name = "APIResponse")
@XmlAccessorType(XmlAccessType.NONE)

public class APIResponse {


    @XmlElement
    private HttpStatus status;

    @XmlElement
    private int statusCode;

    @XmlElement
    private List<ErrorMessage> errorMessages = new ArrayList<>();

    public HttpStatus getStatus ()
    {
        return status;
    }

    public void setStatus (HttpStatus status)
    {
        this.status = status;
    }

    public int getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (int statusCode)
    {
        this.statusCode = statusCode;
    }

    public List<ErrorMessage> getErrorMessages ()
    {
        return errorMessages;
    }

    public void setErrorMessages (List<ErrorMessage> errorMessages)
    {
        this.errorMessages = errorMessages;
    }

    public void addErrorMessage (String msg)
    {
        ErrorMessage error = new ErrorMessage();
        error.setErrorType(ErrorType.Error);
        error.setMessage(msg);
        errorMessages.add(error);
    }

    public void addErrorMessage (List<ErrorMessage> errorMessages)
    {
        getErrorMessages().addAll(errorMessages);
    }

    public boolean hasErrorMessages ()
    {
        return (errorMessages != null && !errorMessages.isEmpty());
    }

    @Override
    public String toString ()
    {
        return "APIResponse [HttpStatus=" + status + ", statusCode=" + statusCode
                + ", errorMessages=" + errorMessages + "]";
    }


}
