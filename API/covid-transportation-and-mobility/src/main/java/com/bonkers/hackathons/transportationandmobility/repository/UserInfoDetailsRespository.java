package com.bonkers.hackathons.transportationandmobility.repository;

import com.bonkers.hackathons.transportationandmobility.models.UserInfoDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoDetailsRespository extends JpaRepository<UserInfoDetails, String> {

}
