// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserDetails _$UserDetailsFromJson(Map<String, dynamic> json) {
  return UserDetails(
    fullName: json['fullName'] as String,
    email: json['email'] as String,
    id: json['id'] as String,
    photoUrl: json['photoUrl'] as String,
    idToken: json['idToken'] as String,
  );
}

Map<String, dynamic> _$UserDetailsToJson(UserDetails instance) =>
    <String, dynamic>{
      'fullName': instance.fullName,
      'email': instance.email,
      'id': instance.id,
      'photoUrl': instance.photoUrl,
      'idToken': instance.idToken,
    };
