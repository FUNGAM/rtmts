package com.bonkers.hackathons.transportationandmobility.models;

import com.google.firebase.auth.FirebaseToken;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserInfo
{
	private String uid;
	private String name;
	private String email;
	private boolean isEmailVerified;
	private String issuer;
	private String picture;

	public static UserInfo init (FirebaseToken decodedToken)
	{
		return UserInfo.builder()
			.uid(decodedToken.getUid())
			.name(decodedToken.getName())
			.email(decodedToken.getEmail())
			.picture(decodedToken.getPicture())
			.issuer(decodedToken.getIssuer())
			.isEmailVerified(decodedToken.isEmailVerified()).build();
	}
}
