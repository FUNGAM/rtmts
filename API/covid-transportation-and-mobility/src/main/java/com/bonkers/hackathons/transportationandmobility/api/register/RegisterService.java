package com.bonkers.hackathons.transportationandmobility.api.register;

import java.util.Map;

public interface RegisterService {

    RegisterResponse registerUser(RegisterUserRequest request);
}
