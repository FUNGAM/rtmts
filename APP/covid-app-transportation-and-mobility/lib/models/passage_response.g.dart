// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'passage_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PassageResponse _$PassageResponseFromJson(Map<String, dynamic> json) {
  return PassageResponse(
    status: json['status'] as String,
    statusCode: json['statusCode'] as int,
    passages: (json['passages'] as List)
        ?.map((e) =>
            e == null ? null : PassageInfo.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$PassageResponseToJson(PassageResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'statusCode': instance.statusCode,
      'passages': instance.passages,
    };
