package com.bonkers.hackathons.transportationandmobility.core;

import com.bonkers.hackathons.transportationandmobility.api.base.APIResponseUtil;
import com.bonkers.hackathons.transportationandmobility.api.device.DeviceStatusResponse;
import com.bonkers.hackathons.transportationandmobility.api.device.DeviceStatusService;
import com.bonkers.hackathons.transportationandmobility.api.device.ValidateDeviceService;
import com.bonkers.hackathons.transportationandmobility.models.UserPassage;
import com.bonkers.hackathons.transportationandmobility.repository.UserPassageRepository;
import com.bonkers.hackathons.transportationandmobility.service.UserSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeviceStatusServiceImpl implements DeviceStatusService {

    @Autowired
    private UserSessionService userSessionService;

    @Autowired
    private ValidateDeviceService validateDeviceService;

    @Autowired
    private UserPassageRepository userPassageRepository;

    @Override
    public DeviceStatusResponse deviceStatus(Integer ePassId, Integer passageId)
    {
        // Declaration
        DeviceStatusResponse response = new DeviceStatusResponse();
        boolean isValidDeviceUserPair;

        isValidDeviceUserPair = validateDeviceService.validateDeviceId(userSessionService.getDeviceId());
        if(!isValidDeviceUserPair) {
            System.out.println("User is not authorized for device to use device: " +
                    userSessionService.getDeviceId() + ", pass: " + ePassId + ", passageId: " + passageId);
            APIResponseUtil.error(response, "You are not authorized to use this device");
            return response;
        }

        if(ePassId == 0 || passageId == 0) {
            System.out.println("Invalid status request passId: " + ePassId + ", passageId: " + passageId);
            APIResponseUtil.error(response, "Invalid status request!");
            return response;
        }
        UserPassage userPassage = userPassageRepository.findByEpassAndPassageId(ePassId, passageId);
        if(null != userPassage) {
            response.setAuthorized(true);
            response.setEpassId(userPassage.getEpassId());
            response.setPassageId(userPassage.getPassageId());
        }
        APIResponseUtil.success(response);
        return response;
    }
}
