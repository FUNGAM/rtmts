package com.bonkers.hackathons.transportationandmobility.util;

public class StringUtil {

    /**
     Determine if a string is null or empty or entirely spaces.

     @param string a String object to check
     @return <b>true</b> if <B>string</B> is null, empty, or all
     spaces; false otherwise
     @aribaapi documented
     */
    public static boolean nullOrEmptyOrBlankString (String string)
    {
        int length = (string != null) ? string.length() : 0;
        if (length > 0) {
            for (int i = 0; i < length; i++) {
                if (!Character.isWhitespace(string.charAt(i))) {
                    return false;
                }
            }
        }
        return true;
    }
}
