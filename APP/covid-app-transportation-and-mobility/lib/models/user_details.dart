import 'package:json_annotation/json_annotation.dart';

part 'user_details.g.dart';

@JsonSerializable()
class UserDetails {
  String fullName;
  String email;
  String id;
  String photoUrl;
  String idToken;

  UserDetails({this.fullName, this.email, this.id, this.photoUrl, this.idToken});

  factory UserDetails.fromJson(Map<String, dynamic> json) => _$UserDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$UserDetailsToJson(this);
}
