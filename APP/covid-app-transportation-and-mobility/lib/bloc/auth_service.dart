import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:transportationandmobility/models/user_details.dart';

class AuthService extends ChangeNotifier {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  UserDetails userDetails;

  AuthService() {
    getUserDetailsFromPref();
  }

  getUserDetailsFromPref() async {
    final pref = await SharedPreferences.getInstance();
    final udp = pref.getString("prefUserDetails");
    if (udp != null) {
      userDetails = UserDetails.fromJson(jsonDecode(udp));
    }
    notifyListeners();
  }

  Future<bool> loginWithGoogle() async {
    try {
      GoogleSignInAccount account = await _googleSignIn.signIn();
      if (account == null) return false;
      userDetails = UserDetails(
        fullName: account.displayName,
        email: account.email,
        photoUrl: account.photoUrl,
        id: account.id,
      );
      AuthResult res = await _auth.signInWithCredential(GoogleAuthProvider.getCredential(
        idToken: (await account.authentication).idToken,
        accessToken: (await account.authentication).accessToken,
      ));
      final pref = await SharedPreferences.getInstance();
      _auth.currentUser().then((user) {
        if (user != null) {
          user.getIdToken().then((token) {
            userDetails.idToken = token.token;
            pref.setString("prefUserDetails", jsonEncode(userDetails.toJson()));
          });
        }
      });
      if (res.user == null) return false;
      return true;
    } catch (e) {
      print(e.message);
      print("Error logging with google");
      return false;
    }
  }

  Future<bool> updateRegistration() async {
    final pref = await SharedPreferences.getInstance();
    pref.setBool("isRegistred", true);
    return true;
  }

  Future<UserDetails> getUserDetails() async {
    if (userDetails == null) {
      await getUserDetailsFromPref();
    }
    return this.userDetails;
  }

  Future<String> getTokenId() async {
    if (userDetails == null) {
      await getUserDetailsFromPref();
    }
    return this.userDetails.idToken;
  }

  Future<void> logOut() async {
    try {
      await _auth.signOut();
    } catch (e) {
      print("error logging out");
    }
  }
}
