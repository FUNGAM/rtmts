package com.bonkers.hackathons.transportationandmobility.controllers;

import com.bonkers.hackathons.transportationandmobility.api.device.DeviceStatusResponse;
import com.bonkers.hackathons.transportationandmobility.api.device.DeviceStatusService;
import com.bonkers.hackathons.transportationandmobility.api.meta.OrganizationsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/device")
public class DeviceResource {

    @Autowired
    private DeviceStatusService deviceStatusService;

    @GetMapping("/status")
    public ResponseEntity deviceStatus (@RequestParam(value = "epassId") Integer epassId,
                                        @RequestParam(value = "passageId") Integer passageId)
    {

        DeviceStatusResponse response = deviceStatusService.deviceStatus(epassId, passageId);

        if (response.getStatus() == HttpStatus.OK) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
