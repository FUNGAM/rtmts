import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';
import 'package:transportationandmobility/pages/home_page.dart';
import 'package:transportationandmobility/pages/login_page.dart';
import 'package:transportationandmobility/pages/splash_page.dart';

import 'bloc/auth_service.dart';
import 'bloc/location_service.dart';
import 'bloc/push_notification_service.dart';
import 'bloc/theme.dart';
import 'bloc/user_registration_service.dart';

GetIt locator = GetIt();

void setupLocator() {
  locator.registerLazySingleton(() => AuthService());
  locator.registerLazySingleton(() => ThemeChanger());
  locator.registerLazySingleton(() => LocationService());
  locator.registerLazySingleton(() => UserRegistrationService());
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  runApp(Covid19UserTravelTracker());
}

class Covid19UserTravelTracker extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    PushNotificationService().initialise(context);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => locator<AuthService>()),
        ChangeNotifierProvider(create: (_) => locator<ThemeChanger>()),
        ChangeNotifierProvider(create: (_) => locator<LocationService>()),
        ChangeNotifierProvider(create: (_) => locator<UserRegistrationService>()),
      ],
      child: MaterialApp(
        title: 'User Commute',
        theme: ThemeData.light(),
        home: MainPage(),
      ),
    );
  }
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ThemeChanger>(context);
    return Container(
      child: StreamBuilder(
        stream: FirebaseAuth.instance.onAuthStateChanged,
        builder: (context, AsyncSnapshot<FirebaseUser> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) return SplashPage();
          if (!snapshot.hasData || snapshot.data == null) return LoginPage();
          return MaterialApp(
            title: 'User Commute',
            theme: theme.getTheme(),
            home: HomePage(),
          );
        },
      ),
    );
  }
}
