package com.bonkers.hackathons.transportationandmobility.api.meta;

import com.bonkers.hackathons.transportationandmobility.models.Organization;
import lombok.Data;

@Data
public class OrganizationInfo {

    private Integer organizationId;
    private String name;
    private String branch;
    private String displayName;
    private float latitude;
    private float longitude;

    public OrganizationInfo() {}

    public OrganizationInfo (Organization organization) {
        this.organizationId = organization.getOrganizationId();
        this.name = organization.getName();
        this.branch = organization.getBranch();
        this.displayName = organization.getDisplayName();
        this.latitude = organization.getGeoLocationLatitude();
        this.longitude = organization.getGeoLocationLongitude();
    }
}
