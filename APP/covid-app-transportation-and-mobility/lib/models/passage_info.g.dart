// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'passage_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PassageInfo _$PassageInfoFromJson(Map<String, dynamic> json) {
  return PassageInfo(
    passageId: json['passageId'] as int,
    city: json['city'] as String,
    taluk: json['taluk'] as String,
    road: json['road'] as String,
    address: json['address'] as String,
    latitude: (json['latitude'] as num)?.toDouble(),
    longitude: (json['longitude'] as num)?.toDouble(),
    radius: (json['radius'] as num)?.toDouble(),
    userAuthorized: json['userAuthorized'] as bool,
  );
}

Map<String, dynamic> _$PassageInfoToJson(PassageInfo instance) =>
    <String, dynamic>{
      'passageId': instance.passageId,
      'city': instance.city,
      'taluk': instance.taluk,
      'road': instance.road,
      'address': instance.address,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'radius': instance.radius,
      'userAuthorized': instance.userAuthorized,
    };
