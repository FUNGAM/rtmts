import 'package:device_id/device_id.dart';
import 'package:http_interceptor/interceptor_contract.dart';
import 'package:http_interceptor/models/request_data.dart';
import 'package:http_interceptor/models/response_data.dart';

import '../main.dart';
import 'auth_service.dart';

class AuthInterceptor implements InterceptorContract {
  @override
  Future<RequestData> interceptRequest({RequestData data}) async {
    AuthService _authDataSource = locator<AuthService>();
    data.headers.addAll({'Authorization': 'Bearer ' + await _authDataSource.getTokenId(), 'deviceId': await DeviceId.getID});
    print('Called +1' + data.url);
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
    if (data.statusCode == 401 && data.body.contains("invalid_token")) {
      throw InvalidTokenException();
    }
    return data;
  }
}

class InvalidTokenException implements Exception {}
