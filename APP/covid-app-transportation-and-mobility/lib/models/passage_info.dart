import 'package:json_annotation/json_annotation.dart';

part 'passage_info.g.dart';

@JsonSerializable()
class PassageInfo {
  int passageId;
  String city;
  String taluk;
  String road;
  String address;
  double latitude;
  double longitude;
  double radius;
  bool userAuthorized;

  PassageInfo({
    this.passageId,
    this.city,
    this.taluk,
    this.road,
    this.address,
    this.latitude,
    this.longitude,
    this.radius,
    this.userAuthorized,
  });

  factory PassageInfo.fromJson(Map<String, dynamic> json) => _$PassageInfoFromJson(json);

  Map<String, dynamic> toJson() => _$PassageInfoToJson(this);
}
