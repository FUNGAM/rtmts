// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserRequest _$UserRequestFromJson(Map<String, dynamic> json) {
  return UserRequest(
    phoneNumber: json['phoneNumber'] as String,
    deviceId: json['deviceId'] as String,
    organizationId:
        (json['organizationId'] as List)?.map((e) => e as int)?.toList(),
  );
}

Map<String, dynamic> _$UserRequestToJson(UserRequest instance) =>
    <String, dynamic>{
      'phoneNumber': instance.phoneNumber,
      'deviceId': instance.deviceId,
      'organizationId': instance.organizationId,
    };
