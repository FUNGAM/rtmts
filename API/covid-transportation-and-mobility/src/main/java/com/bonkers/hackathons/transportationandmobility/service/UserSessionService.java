package com.bonkers.hackathons.transportationandmobility.service;

import com.bonkers.hackathons.transportationandmobility.models.CurrentUserSession;
import com.bonkers.hackathons.transportationandmobility.models.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserSessionService
{
    private ThreadLocal<CurrentUserSession> userLocal = new ThreadLocal<>();

    public String getUserId ()
    {
        log.debug("getUserId: START");
        CurrentUserSession session = userLocal.get();
        if (session != null) {
            log.debug("session: " + session);
            return session.getUserInfo().getUid();
        }
        throw new RuntimeException("User info not fount");
    }

    public String getDeviceId ()
    {
        CurrentUserSession session = userLocal.get();
        if (session != null) {
            return session.getDeviceId();
        }
        throw new RuntimeException("DeviceId not fount");
    }

    public UserInfo getUserInfo ()
    {
        log.debug("getUserInfo: START");
        CurrentUserSession session = userLocal.get();
        if (session != null) {
            log.debug("session: " + session);
            return session.getUserInfo();
        }
        throw new RuntimeException("User info not fount");
    }

    public void initSession (UserInfo details, String deviceId) {
        if(details == null) {
            return;
        }
        CurrentUserSession session = userLocal.get();
        if(session == null) {
            session = new CurrentUserSession();
            userLocal.set(session);
        }
        session.setUserInfo(details);
        session.setDeviceId(deviceId);
        log.debug("initSession session: " + session);
    }
}
