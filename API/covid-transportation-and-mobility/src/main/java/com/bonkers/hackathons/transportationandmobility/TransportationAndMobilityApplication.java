package com.bonkers.hackathons.transportationandmobility;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransportationAndMobilityApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransportationAndMobilityApplication.class, args);
	}
}
