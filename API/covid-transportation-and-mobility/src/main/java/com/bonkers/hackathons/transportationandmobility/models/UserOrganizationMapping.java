package com.bonkers.hackathons.transportationandmobility.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name = "USER_ORGANIZATION_MAPPING")
@Data
public class UserOrganizationMapping {

    @Id
    @GeneratedValue
    private Integer userOrgMappingId;
    private Integer epassId;
    private Integer organizationId;
    private String userId;
    private boolean active;
    private long createdOn;
    private String createdBy;
    private long modifiedOn;
    private String modifiedBy;


}
