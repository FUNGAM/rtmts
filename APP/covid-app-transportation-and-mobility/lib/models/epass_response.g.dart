// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'epass_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EpassResponse _$EpassResponseFromJson(Map<String, dynamic> json) {
  return EpassResponse(
    status: json['status'] as String,
    statusCode: json['statusCode'] as int,
    epassId: json['epassId'] as int,
  );
}

Map<String, dynamic> _$EpassResponseToJson(EpassResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'statusCode': instance.statusCode,
      'epassId': instance.epassId,
    };
