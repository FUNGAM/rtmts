import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  GoogleMapController _controller;
  static const double MAP_ZOOM = 14.5;
  Set<Marker> _markers = HashSet<Marker>();
  BitmapDescriptor _currLocIcon;

  @override
  initState() {
    super.initState();
    _setCurrLocIcon();
  }

  _setCurrLocIcon() async {
    _currLocIcon = await BitmapDescriptor.fromAssetImage(ImageConfiguration(), "assets/images/marker.png");
    _markers.add(
      Marker(
        markerId: MarkerId("0"),
        position: LatLng(12.980078, 77.591618),
        infoWindow: InfoWindow(title: "Current Location"),
        icon: _currLocIcon,
      ),
    );
  }

  CameraPosition _currentLocation = CameraPosition(
    target: LatLng(12.980078, 77.591618),
    zoom: MAP_ZOOM,
  );

  void _onMapCreate(GoogleMapController controller) {
    this._controller = controller;
    setMapStyle();
    _viewCurrentLocation();
  }

  void _getCurrentLocationMarker() {
    setState(() {
      _markers.add(
        Marker(
          markerId: MarkerId("currentlocation"),
          position: _currentLocation.target,
          infoWindow: InfoWindow(title: "Current Location"),
          icon: _currLocIcon,
        ),
      );
    });
  }

  void setMapStyle() async {
    String style = await DefaultAssetBundle.of(context).loadString('assets/maps-style.json');
    _controller.setMapStyle(style);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Stack(
        children: <Widget>[
          GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: _currentLocation,
            onMapCreated: _onMapCreate,
            markers: _markers,
          ),
          Container(
            alignment: Alignment.bottomCenter,
            padding: EdgeInsets.only(bottom: 10),
            child: Text(
              "Bonkers",
              style: TextStyle(color: Colors.grey),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
              alignment: Alignment.topRight,
              child: Column(
                children: <Widget>[
                  FloatingActionButton(
                    onPressed: _viewCurrentLocation,
                    child: Icon(Icons.my_location),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _viewCurrentLocation() async {
    await _setCurrentLocation();
    _getCurrentLocationMarker();
    _controller.animateCamera(CameraUpdate.newCameraPosition(_currentLocation));
  }

  _setCurrentLocation() async {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best).then((Position position) async {
      _currentLocation = CameraPosition(
        target: LatLng(position.latitude, position.longitude),
        zoom: MAP_ZOOM,
      );
    }).catchError((e) {
      print(e);
    });
  }
}
