package com.bonkers.hackathons.transportationandmobility.repository;

import com.bonkers.hackathons.transportationandmobility.models.Organization;
import com.bonkers.hackathons.transportationandmobility.models.UserInfoDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrganizationRepository extends JpaRepository<Organization, Long> {
}
