package com.bonkers.hackathons.transportationandmobility.controllers;

import com.bonkers.hackathons.transportationandmobility.api.register.RegisterResponse;
import com.bonkers.hackathons.transportationandmobility.api.register.RegisterService;
import com.bonkers.hackathons.transportationandmobility.api.register.RegisterUserRequest;
import com.bonkers.hackathons.transportationandmobility.models.UserInfoDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/register")
public class RegisterResource {

    @Autowired
    private RegisterService registerService;

    @GetMapping("/admin")
    public Map<String, Object> regiterPoliceAdmin() {
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("message", "please contact your police dept for your registration");
        return response;
    }

    @PostMapping("/user")
    public ResponseEntity registerUser(@RequestBody RegisterUserRequest request) {

        RegisterResponse response = registerService.registerUser(request);

        if (response.getStatus() == HttpStatus.OK) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

}
