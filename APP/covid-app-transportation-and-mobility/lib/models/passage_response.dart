import 'package:json_annotation/json_annotation.dart';
import 'package:transportationandmobility/models/passage_info.dart';

part 'passage_response.g.dart';

@JsonSerializable()
class PassageResponse {
  String status;
  int statusCode;
  List<PassageInfo> passages;

  PassageResponse({this.status, this.statusCode, this.passages});

  factory PassageResponse.fromJson(Map<String, dynamic> json) => _$PassageResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PassageResponseToJson(this);
}
