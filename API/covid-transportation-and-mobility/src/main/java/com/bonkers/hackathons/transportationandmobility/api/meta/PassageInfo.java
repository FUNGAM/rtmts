package com.bonkers.hackathons.transportationandmobility.api.meta;

import com.bonkers.hackathons.transportationandmobility.models.Passage;
import lombok.Data;

@Data
public class PassageInfo {

    private Integer passageId;
    private String city;
    private String taluk;
    private String road;
    private String address;
    private float latitude;
    private float longitude;
    private float radius;
    private boolean userAuthorized;

    public PassageInfo() {}

    public PassageInfo(Passage passage) {

        this.passageId = passage.getPassageId();
        this.city = passage.getCity();
        this.taluk = passage.getTaluk();
        this.road = passage.getRoad();
        this.address = passage.getAddress();
        this.latitude = passage.getGeoLocationLatitude();
        this.longitude = passage.getGeoLocationLongitude();
        this.radius = passage.getGeoLocationRadius();
    }
}
