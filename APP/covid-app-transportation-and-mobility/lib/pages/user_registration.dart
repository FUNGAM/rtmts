import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:transportationandmobility/bloc/location_service.dart';
import 'package:transportationandmobility/bloc/user_registration_service.dart';
import 'package:transportationandmobility/models/organization.dart';
import 'package:transportationandmobility/utils/sh_navigator.dart';

import '../main.dart';

class UserRegistration extends StatefulWidget {
  @override
  _UserRegistrationState createState() => _UserRegistrationState();
}

class _UserRegistrationState extends State<UserRegistration> {
  final formkey = GlobalKey<FormState>();

  @override
  void initState() {
    UserRegistrationService service = locator<UserRegistrationService>();
    service.getOrganizations();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
              width: double.infinity,
              child: Form(
                key: formkey,
                child: Column(mainAxisAlignment: MainAxisAlignment.center, children: buildInputs()),
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> buildInputs() {
    UserRegistrationService userRegService = Provider.of<UserRegistrationService>(context);
    LocationService locationService = Provider.of<LocationService>(context);
    List<Widget> fields = [
      TextFormField(
        keyboardType: TextInputType.phone,
        cursorColor: Colors.black,
        decoration: buildInputDecoration('Phone Number'),
        onChanged: (String value) => userRegService.phoneNumber = value,
      ),
      SizedBox(height: 20),
      Consumer<UserRegistrationService>(
        builder: (context, urs, _) {
          return DropdownButton<Organization>(
            isExpanded: true,
            hint: Text('Choose Organisation'),
            value: urs.org != null ? urs.org : Organization(),
            icon: Icon(Icons.location_city),
            onChanged: (Organization newValue) {
              userRegService.org = newValue;
            },
            items: userRegService.orgs.map<DropdownMenuItem<Organization>>((Organization value) {
              return DropdownMenuItem<Organization>(
                value: value,
                child: Text(value.name),
              );
            }).toList(),
          );
        },
      ),
      SizedBox(height: 20),
      InkWell(
        onTap: () {
          userRegService.registerUserAndGeneratePass().then((res) {
            locationService.initAllGiofence(userRegService.ePassId).then(
              (res) {
                ShNavigator.goToTripManagePage(context, userRegService.ePassId);
              },
            );
          });
        },
        child: Container(
          height: 50,
          width: 1000,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            color: locationService.tripStarted ? Colors.greenAccent[700] : Colors.blueAccent[700],
          ),
          child: Text(
            "Request Pass",
            style: TextStyle(
              letterSpacing: 3,
              fontSize: 13,
              color: Colors.white,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
      ),
    ];
    return fields;
  }

  InputDecoration buildInputDecoration(String hint) {
    return InputDecoration(
      labelText: hint,
      contentPadding: EdgeInsets.fromLTRB(30, 15, 30, 15),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}
