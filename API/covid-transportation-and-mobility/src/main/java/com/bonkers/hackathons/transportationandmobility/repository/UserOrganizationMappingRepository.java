package com.bonkers.hackathons.transportationandmobility.repository;

import com.bonkers.hackathons.transportationandmobility.models.UserInfoDetails;
import com.bonkers.hackathons.transportationandmobility.models.UserOrganizationMapping;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserOrganizationMappingRepository extends JpaRepository<UserOrganizationMapping, Long> {
}
