// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'organization.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Organization _$OrganizationFromJson(Map<String, dynamic> json) {
  return Organization(
    organizationId: json['organizationId'] as int,
    name: json['name'] as String,
    displayName: json['displayName'] as String,
    branch: json['branch'] as String,
    latitude: (json['latitude'] as num)?.toDouble(),
    longitude: (json['longitude'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$OrganizationToJson(Organization instance) =>
    <String, dynamic>{
      'organizationId': instance.organizationId,
      'name': instance.name,
      'displayName': instance.displayName,
      'branch': instance.branch,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };
