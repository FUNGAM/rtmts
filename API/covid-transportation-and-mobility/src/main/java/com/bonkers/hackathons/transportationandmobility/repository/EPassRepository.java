package com.bonkers.hackathons.transportationandmobility.repository;

import com.bonkers.hackathons.transportationandmobility.models.EPass;
import com.bonkers.hackathons.transportationandmobility.models.UserPassage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EPassRepository extends JpaRepository<EPass, Long> {

    String TableName = "EPass";

    String findByUserId = "select e from " + TableName +
                    " e where e.userId = :userId and e.active = true";

    @Query(findByUserId)
    EPass findByUserId (@Param("userId") String userId);
}
