import 'package:json_annotation/json_annotation.dart';

part 'user_location.g.dart';

@JsonSerializable()
class UserLocation {
  double latitude;
  double longitude;
  double heading;
  double accuracy;

  UserLocation({this.latitude, this.longitude, this.heading, this.accuracy});

  factory UserLocation.fromJson(Map<String, dynamic> json) => _$UserLocationFromJson(json);

  Map<String, dynamic> toJson() => _$UserLocationToJson(this);
}
