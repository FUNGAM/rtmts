import 'package:json_annotation/json_annotation.dart';

part 'epass_response.g.dart';

@JsonSerializable()
class EpassResponse {
  String status;
  int statusCode;
  int epassId;

  EpassResponse({this.status, this.statusCode, this.epassId});

  factory EpassResponse.fromJson(Map<String, dynamic> json) => _$EpassResponseFromJson(json);

  Map<String, dynamic> toJson() => _$EpassResponseToJson(this);
}
