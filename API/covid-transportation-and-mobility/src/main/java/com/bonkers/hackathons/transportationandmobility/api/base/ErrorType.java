package com.bonkers.hackathons.transportationandmobility.api.base;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ErrorType")
@XmlEnum
public enum ErrorType {

    @XmlEnumValue("Error")
    Error,
    @XmlEnumValue("Warning")
    Warning;

}
