package com.bonkers.hackathons.transportationandmobility.config.security;

import com.bonkers.hackathons.transportationandmobility.helpers.RequestHelper;
import com.bonkers.hackathons.transportationandmobility.models.UserInfo;
import com.bonkers.hackathons.transportationandmobility.service.UserSessionService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class UserSessionFilter extends OncePerRequestFilter {

	@Autowired
	RequestHelper requestHelper;

	@Autowired
	UserSessionService userSessionService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String idToken = requestHelper.getTokenFromRequest(request);
		FirebaseToken decodedToken = null;
		if (idToken != null && !idToken.equalsIgnoreCase("undefined")) {
			try {
				decodedToken = FirebaseAuth.getInstance().verifyIdToken(idToken);
			} catch (FirebaseAuthException e) {
				log.error("Firebase Exception:: ", e.getLocalizedMessage());
			}
		}
		if (decodedToken != null) {
			UserInfo userInfo = UserInfo.init(decodedToken);
			userSessionService.initSession(userInfo, request.getHeader("deviceId"));
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
				userInfo,
					decodedToken, null);
			authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
			SecurityContextHolder.getContext().setAuthentication(authentication);

		}
		filterChain.doFilter(request, response);
	}
}
