import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:transportationandmobility/bloc/auth_service.dart';
import 'package:transportationandmobility/bloc/user_registration_service.dart';
import 'package:transportationandmobility/helpers/helpers.dart';
import 'package:transportationandmobility/pages/user_registration.dart';
import 'package:transportationandmobility/utils/sh_navigator.dart';

import '../main.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthService>(context);
    UserRegistrationService service = locator<UserRegistrationService>();
    service.getOrganizations();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          "User Commute",
          style: TextStyle(
            letterSpacing: 3,
            fontSize: 16,
            color: Colors.blueAccent,
            fontWeight: FontWeight.w900,
          ),
        ),
        actions: <Widget>[
          InkWell(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Helper.geAvatar(authProvider.userDetails),
            ),
            onTap: () => ShNavigator.goToSettingsPage(context),
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          UserRegistration(),
        ],
      ),
    );
  }
}
