package com.bonkers.hackathons.transportationandmobility.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name = "PASSAGE", indexes = { @Index(name = "UNQPASSAGE", columnList = "passageId", unique = true),
        @Index(name = "UNQLOCATION", columnList = "geoLocationLatitude, geoLocationLongitude, geoLocationRadius", unique = true)
})
@Data
public class Passage {

    @Id
    private Integer passageId;
    private String country;
    private String state;
    private String district;
    private String city;
    private String taluk;
    private String address;
    private String road;
    private float geoLocationLatitude;
    private float geoLocationLongitude;
    private float geoLocationRadius;
    private boolean active;
    private long createdOn;
    private String createdBy;
    private long modifiedOn;
    private String modifiedBy;

}
