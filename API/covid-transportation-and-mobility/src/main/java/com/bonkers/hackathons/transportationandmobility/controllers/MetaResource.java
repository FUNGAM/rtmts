package com.bonkers.hackathons.transportationandmobility.controllers;

import com.bonkers.hackathons.transportationandmobility.api.meta.MetaService;
import com.bonkers.hackathons.transportationandmobility.api.meta.OrganizationsResponse;
import com.bonkers.hackathons.transportationandmobility.api.meta.PassagesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/meta")
public class MetaResource {

    @Autowired
    private MetaService metaService;

    @GetMapping("/organizations")
    public ResponseEntity getAllRegisteredOrganizations() {

        OrganizationsResponse response = metaService.getAllRegisteredOrganizations();

        if (response.getStatus() == HttpStatus.OK) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/passages")
    public ResponseEntity getAllAndUserPassages() {

        PassagesResponse response = metaService.getAllAndUserPassages();

        if (response.getStatus() == HttpStatus.OK) {
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
