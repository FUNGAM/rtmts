import 'package:json_annotation/json_annotation.dart';

part 'organization.g.dart';

@JsonSerializable()
class Organization {
  int organizationId;
  String name;
  String displayName;
  String branch;
  double latitude;
  double longitude;

  Organization({this.organizationId, this.name, this.displayName, this.branch, this.latitude, this.longitude});

  factory Organization.fromJson(Map<String, dynamic> json) => _$OrganizationFromJson(json);

  Map<String, dynamic> toJson() => _$OrganizationToJson(this);
}
